.. highlight:: shell

============
Installation
============


Stable release
--------------

To install learn-drama, run this command in your terminal:

.. code-block:: console

    $ pip3 install learn_drama

This is the preferred method to install learn-drama, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for learn-drama can be downloaded from the `Github repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://gitlab.com/goethe_faust/learn_drama


Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python3 setup.py install


.. _Github repo: https://gitlab.com/goethe_faust/learn_drama
