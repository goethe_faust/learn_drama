"""Top-level package for learn-drama."""

__author__ = """Stefan Bollmann"""
__email__ = 'stefan.bollmann@rwth-aachen.de'
__version__ = '0.0.1'

from .learn_drama import DramaLearner  # noqa: F401
