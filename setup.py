#!/usr/bin/env python

"""The setup script."""

from setuptools import setup, find_packages

with open('README.rst') as readme_file:
    readme = readme_file.read()

with open('HISTORY.rst') as history_file:
    history = history_file.read()

requirements = ['Click>=7.0', ]

test_requirements = ['pytest>=3', ]

setup(
    author="Stefan Bollmann",
    author_email='stefan.bollmann@rwth-aachen.de',
    python_requires='>=3.6',
    classifiers=[
        'Development Status :: 2 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Natural Language :: English',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.8',
    ],
    description="Type and learn drama helper.",
    long_description_content_type="text/markdown",
    entry_points={
        'console_scripts': [
            'learn_drama=learn_drama.cli:main',
        ],
    },
    install_requires=requirements,
    license="GNU General Public License v3",
    long_description=readme + '\n\n' + history,
    include_package_data=True,
    keywords='learn_drama',
    name='learn_drama',
    packages=find_packages(include=['learn_drama', 'learn_drama.*']),
    test_suite='tests',
    tests_require=test_requirements,
    url='https://gitlab.com/goethe_faust/learn_drama',
    version='0.0.1.a',
    zip_safe=False,
)
