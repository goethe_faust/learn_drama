#!/usr/bin/env python3
import sys

from learn_drama import cli

def main():
    cli.main()
    return 0


if __name__ == '__main__':
    sys.exit(main())
